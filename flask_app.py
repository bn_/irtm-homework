from flask import Flask, render_template, request
from index import index, get_dict_count, get_line_count, process_query, fetch_tweet, unpickle, read_glossary
import logging

app = Flask(__name__)
glossary = read_glossary("static/english-words")
logging.getLogger().setLevel(20)

@app.route('/count')
def count():
    return "Dictionary size: %d<br>Tweets indexed: %d"%(get_dict_count(),get_line_count())
	
def check_if_reindexing_required():
	if get_dict_count()==0:
        #in case of a server restart or app crash
        logging.info("Dictionary empty, fetching from cache")
        unpickle("static/tweets")
    if get_dict_count()==0:
        logging.info("Dictionary still empty, reindexing")
        index("static/tweets",-1,10000) 

@app.route('/')
def query_form():
	check_if_reindexing_required()
    return render_template('query.html')

@app.route('/query', methods=['GET'])
def results():
    query_text = request.args.get("query")
    spellcheck = request.args.get("spellcheck")=="yes"
    search_terms, result_indices = process_query(query_text, spellcheck, glossary)
    results = [fetch_tweet(i) for i in result_indices]
    return render_template('results.html', query = str(search_terms), total = len(results), results = results)


